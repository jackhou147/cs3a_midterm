#include <iostream>
#include <fraction.h>

using namespace std;

string boolean(bool);

int main()
{
    //stringstream ss_x("1"), ss_y("2");

    Fraction x,y,ans;
    cout << "x: ";
    cin >> x;
    cout << "y: ";
    cin >> y;
    x = y;
    cout << "x after = y: " << x<< endl;

//    cout << "x ==y: " << boolean(x == y) << endl;
//    cout << "x != y: " << boolean(x != y) << endl;
//    cout << "x < y: " << boolean(x < y) << endl;
//    cout << "x >y: " << boolean(x > y) << endl;
//    cout << "x <= y: " << boolean(x <= y) << endl;
//    cout << "x >=y: " << boolean(x >= y) << endl;

    return 0;
}

string boolean(bool bul)
{
    string ans("false");
    if(bul) ans = "true";
    return ans;
}
