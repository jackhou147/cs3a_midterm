TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    polynomial.cpp\
    ../Term/term.cpp\
    ../Fraction/fraction.cpp\
    ../Fraction/fractionfriends.cpp \


HEADERS += \
    polynomial.h\
    term_error.h

