#include <iostream>
#include "../Fraction/fraction.h"
#include "term.h"
using namespace std;

string boolean(bool);
int main()
{

//    TermVal x(
//                Fraction(4,3),
//                Fraction(46,23),
//                Fraction(10,1)
//                );
//    TermVal y(
//                Fraction(4,3),
//                Fraction(46,23),
//                Fraction(10,1)
//                );

//    cout << "x > y: " << boolean(x > y) << endl;

    stringstream x_ss("-4/2 x ^+3/9"), y_ss("+4");
    Term x,y;
    x_ss >> x;
    y_ss >> y;
    cout << "x: " << x << endl;
    cout << "y: " << y << endl;
    cout << "x+y = " << x+y << endl;
    cout << "x-y = " << x-y << endl;
    cout << "x*y = " << x*y << endl;
//    cout << "x: ";
//    cin >> x;
//    cout << "y: ";
//    cin >> y;
//    cout << "x: " << x << endl
//         << "y: " << y << endl;






//    TermExpr x(Fraction(1,2), Fraction(3,4));
//    cout << x << endl;
//    x = x.differentiate();
//    cout << "x'" << x << endl;
//    cout << "Hello World!" << endl;
    return 0;
}


string boolean(bool bul)
{
    string ans("false");
    if(bul) ans = "true";
    return ans;
}
