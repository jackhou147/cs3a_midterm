#include "expressionmanager.h"


ExpressionManager::ExpressionManager()
{
    for(int i=0; i<MAX_LIST_SIZE; i++)
    {
        const Expression x;
        list.push_back(x);
    }
}

ExpressionManager::~ExpressionManager()
{

}

void ExpressionManager::load(string fileName)
{
    /***
     * load expressions from a file
     * @pre: file must be in following format:
     *  [valid expression][\n]
     *  [valid expression][\n]
     *  [valid expression][\n]
     */
    vector<Expression> ans;
    string _fileName = fileName;
    if(_fileName.find(".exp") == string::npos)
    {
        _fileName += ".exp";
    }
    cout << "fileName: " << _fileName << endl;
    ifstream myFile;
    myFile.open(_fileName);
    while(ans.size() < MAX_LIST_SIZE)
    {
        Expression expr;
        myFile >> expr;
        ans.push_back(expr);
        if(myFile.eof()) break;
    }

    for(int i=ans.size(); i<MAX_LIST_SIZE; i++)
    {
        const Expression x;
        ans.push_back(x);
    }

    list = ans;
}

void ExpressionManager::save(string fileName)
{
    /***
     * save all expressions to a file
     */
    string _fileName = fileName;
    if(_fileName.find(".exp") == string::npos)
    {
        _fileName += ".exp";
    }
    ofstream myFile;
    myFile.open(_fileName);
    for(int i=0; i<list.size(); i++)
        myFile << list[i] << "\n";
    myFile.close();
}

ExpressionManager ExpressionManager::operator << (string &expr)
{
    /***
     * @brief add a new expression into list
     * @pre string must be in the following format:
     *  "[LETTER A-Z][space][=][space][VALID EXPRESSION]"
     * @example "F = 2x + 4"
     */

    //if(list.size()+1 > MAX_LIST_SIZE) throw LIST_FULL;
    stringstream ss;
    char junk;
    char which = expr[0];
    Expression x;
    //cout << "expr: " << expr << endl;
    expr.erase(0, 4);
    ss << expr;
    ss >> x;
    //cout << "about to push to list" << endl;
    //cout << tolower(which)-'a'+100 << endl;
    list[tolower(which)-'a'] = x;
    //cout << which << " position just pushed in: " << x << endl;
    return *this;
}

Expression ExpressionManager::operator[] (char& which)
{
    /***
     * return one of the expressions from the list
     * @param which: char A-Z
     */
    which = tolower(which);
    if((which-'a') > list.size())
    {
        throw FUNCTION_NOT_FOUND;
    }
    //cout << "which-a: " << which-'a' << endl;
    return list[which-'a'];
}

void ExpressionManager::set(char& which, const Expression& what)
{
    //cout << "set fired!! " << endl;
    //cout << "which: " << which;
    which = tolower(which);
    //cout << which - 'a' << endl;
    list[which - 'a'] = what;
    //cout << "new which: " << list[which-'a'] << endl;
}
