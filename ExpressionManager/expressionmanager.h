#ifndef EXPRESSIONMANAGER_H
#define EXPRESSIONMANAGER_H

#include <vector>
#include <fstream>
#include <iostream>
#include "../Expression/expression.h"

using namespace std;

enum ERRORS{LIST_FULL, FUNCTION_NOT_FOUND, CANNOT_OPEN_FILE};

class ExpressionManager
{
public:
    ExpressionManager();
    ~ExpressionManager();

    //getter/setter
    void set(char& which, const Expression& what);

    //file io
    void load(string fileName);
    void save(string fileName);

    //overloaded operators
    ExpressionManager operator << (string& expr);    //insertion operator
    Expression operator[] (char&);   //return an expression

private:
    vector<Expression> list;
    const int MAX_LIST_SIZE = 26;
};





#endif // EXPRESSIONMANAGER_H
