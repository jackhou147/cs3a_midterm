TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    expressionmanager.cpp\
    ../Term/term.cpp\
    ../Fraction/fraction.cpp\
    ../Fraction/fractionfriends.cpp\
    ../Expression/expression.cpp

HEADERS += expressionmanager.h \
    ../Expression/term_error.h \
    ../Expression/expression.h\
